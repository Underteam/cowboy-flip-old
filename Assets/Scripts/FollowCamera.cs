﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {
    private CameraFollow cameraFollow;

    private void Awake () {
        cameraFollow = FindObjectOfType<CameraFollow> ();
    }

    // Update is called once per frame
    void Update () {
        transform.position = new Vector3 (cameraFollow.transform.position.x, transform.position.y, cameraFollow.transform.position.z);
    }
}