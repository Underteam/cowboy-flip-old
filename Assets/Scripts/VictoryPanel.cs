﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryPanel : MonoBehaviour {

    [Header ("Задержка перед появлением")]
    public float appearDelay = 1;

    [Header ("Панель с задачами")]
    public Transform objectivesPanel;

    [Header ("Панель когда все задачи сделаны")]
    public Transform onTargetsDonePanel;

    [Header ("Панель с инфой для прохождения")]
    public FormattedText infoAbountConditionForNextLevel;

    [Header ("Задержка появления галочек")]
    public float markAppearDelayed = 0.2f;

    [Header ("Задержка появления кнопки")]
    public float buttonAppearDelayed = 0.2f;
    private List<ObjectiveUI> objectiveUIs = new List<ObjectiveUI> ();

    public Button nextLevel;
    public void Init (LevelGenerator levelGenerator, GameController gameController) {

        StopAllCoroutines ();

        nextLevel.interactable = false;
        objectiveUIs = new List<ObjectiveUI> (objectivesPanel.GetComponentsInChildren<ObjectiveUI> ());
        bool isWin = true;
        LevelGenerator.WinCondition[] conditions = levelGenerator.WinConditions;
        bool afterZero = false;

        onTargetsDonePanel.gameObject.SetActive (false);

        int lastOpened = 0;
        int markCount = 0;
        int conditionsCount = 0;
        bool isNeedToWinExist = false;

        float sumDelay = 0;
        for (int i = 0; i < conditions.Length; i++) {
            objectiveUIs[i].mark.gameObject.SetActive (false); //сначала отключаем

            if (conditions[i].flipCount <= 0) { //если задача пустая, то все последующие надо обнулить
                afterZero = true;
            }

            if (afterZero) { //после нуля обнуляем
                conditions[i].flipCount = 0;
                conditions[i].needForWin = false;
                objectiveUIs[i].SetActive (false);
            } else {

                int index = i;
                lastOpened = index;
                objectiveUIs[i].conditionText.text = conditions[i].flipCount + " flips";
                conditionsCount++;
                if (gameController.flipCount >= conditions[i].flipCount) //если количество кручений столько сколько надо
                //делаем появление галочки
                {
                    float delay = markAppearDelayed * (index + 1);
                    sumDelay = delay;
                    StartCoroutine (DoSmthDelayed (delay,
                        delegate {
                            objectiveUIs[index].mark.gameObject.SetActive (true);
                        }));

                    markCount++;
                } else {
                    //не засчитываем победу, если данная задача была необходима
                    if (conditions[i].needForWin) {
                        isWin = false;
                    }
                }
            }

            if (conditions[i].needForWin)
                isNeedToWinExist = true;
        }

        //делаем сначала кнопку следующего уровня не активной
        nextLevel.interactable = false;

        float afterDelay = markCount * markAppearDelayed + buttonAppearDelayed;
        // Debug.LogError (afterDelay);

        //в случае победы активируем кнопку
        if (isWin) {
            // Debug.LogError (lastOpened);
            // if (isNeedToWinExist) {
            StartCoroutine (DoSmthDelayed (afterDelay,
                delegate {
                    // Debug.LogError ("Show");
                    nextLevel.interactable = true;
                    nextLevel.GetComponent<Animator> ().SetTrigger ("Appear");
                    if (markCount == conditionsCount) {
                        onTargetsDonePanel.gameObject.SetActive (true);
                        infoAbountConditionForNextLevel.gameObject.SetActive (false);
                    }
                }));

            // } else {
            //     nextLevel.interactable = true;
            // }
        }

    }

    public void SetInfoAboutCondition (LevelGenerator levelGenerator) {
        LevelGenerator.WinCondition[] conditions = levelGenerator.WinConditions;
        int needForWinIndex = -1;
        for (int i = 0; i < conditions.Length; i++) {
            if (conditions[i].needForWin)
                needForWinIndex = i;
        }

        if (needForWinIndex >= 0) {
            if (infoAbountConditionForNextLevel.values.Count > 0)
                infoAbountConditionForNextLevel.values[0] = conditions[needForWinIndex].conditionName;
        } else {
            infoAbountConditionForNextLevel.gameObject.SetActive (false);
        }
    }

    public static IEnumerator DoSmthDelayed (float time, System.Action action) {
        yield return new WaitForSeconds (time);
        if (action != null)
            action.Invoke ();
    }

    public void Show () {
        transform.localScale = Vector3.one;
    }

    public void Hide () {
        transform.localScale = Vector3.zero;
    }
}