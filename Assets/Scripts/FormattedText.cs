﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Text))]
[ExecuteInEditMode]
public class FormattedText : MonoBehaviour {

	private Text text;

	[Header ("Надо добавить локализацию")]
	public string localizationKey = "";

	[Header ("Формат")]
	public string format = "{0}";
	public List<string> values = new List<string> ();

	private void Start () {
		UpdateText ();
	}

	private void OnValidate () {
		UpdateText ();
	}

	public void UpdateText () {
		if (text == null)
			text = GetComponent<Text> ();
		try {
			text.text = string.Format (format, values.ToArray ());
			cachedText = text.text;
		} catch (System.Exception ex) { Debug.Log (ex); }
	}

	string cachedText = "";
	private void Update () {
		if (text == null)
			text = GetComponent<Text> ();

		// if (cachedText != text.text) {
		UpdateText ();
		// }
	}
}