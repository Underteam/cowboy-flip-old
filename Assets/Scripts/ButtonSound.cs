﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Button))]
[DisallowMultipleComponent]
public class ButtonSound : MonoBehaviour {
    public StegoAudioClip clip;
    private void Awake () {
        GetComponent<Button> ().onClick.AddListener (delegate { if (AudioManager.instance != null) AudioManager.instance.PlaySound (clip); });
    }
}