﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class TransparencyPanel : MonoBehaviour {

	[Range (0, 1)]
	public float transparency = 0;
	public Gradient colorGradient;
	public bool useColorGradient;
	public float alpha {
		set {
			transparency = value;
			UpdateTransparency ();
		}
	}

	public List<Transform> ignoreList = new List<Transform> ();
	public List<MaskableSettings> savedGraphics = new List<MaskableSettings> ();

	[System.Serializable]
	public class MaskableSettings {
		public MaskableGraphic mask;

		[Range (0, 1)]
		public float maxTransparency;

		public MaskableSettings (MaskableGraphic masc, float maxTransparency) {
			this.mask = masc;
			this.maxTransparency = maxTransparency;
		}

		public MaskableSettings () {

		}
	}

	// private void OnValidate () {
	// 	UpdateTransparency (transform.GetComponentsInChildren<MaskableGraphic> ());
	// }

	private void Awake () {
		UpdateTransparency ();
	}

	[EditorButton]
	public void AddGraphics () {
		MaskableGraphic[] graphics = GetComponentsInChildren<MaskableGraphic> (true);
		for (int i = 0; i < graphics.Length; i++) {
			if (savedGraphics.FindIndex ((g) => g.mask == graphics[i]) < 0) {
				savedGraphics.Add (new MaskableSettings (graphics[i], graphics[i].color.a));
			}
		}
	}

	[EditorButton]
	public void ClearGraphics () {
		savedGraphics = new List<MaskableSettings> ();
	}

	public void UpdateTransparency () {
		for (int i = 0; i < savedGraphics.Count; i++) {
			if (savedGraphics[i] != null)
				if (!ignoreList.Contains (savedGraphics[i].mask.transform)) {
					Color color = (!useColorGradient) ? savedGraphics[i].mask.color : colorGradient.Evaluate (transparency);
					savedGraphics[i].mask.color = new Color (color.r, color.g, color.b, Mathf.Min (transparency, savedGraphics[i].maxTransparency));
				}
		}
	}

	private void OnValidate () {
		UpdateTransparency ();
	}

	public void SetAlpha (float val) {
		alpha = val;
	}

	public void SetAlwaysUpdate (bool mode) {
		alwaysUpdate = mode;
	}

	public bool alwaysUpdate = true;
	float lastTrancparency;
	private void Update () {

		if (alwaysUpdate) {
			// if (Application.isPlaying) {
			if (transparency != lastTrancparency) {
				UpdateTransparency ();
				// }
				lastTrancparency = transparency;
			}
		}
	}
}