﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent (typeof (Animator))]
public class HorseScript : MonoBehaviour {

    public PlayerController playerController;
    public Transform sittingPos;
    public float runTime = 5;
    Animator anim;
    public static HorseScript instance = null;

    private void Awake () {
        if (Application.isPlaying)
            anim = GetComponent<Animator> ();
        instance = this;
    }

    private Vector3 startPos;
    private void Start () {
        startPos = transform.position;
    }
    public void Restart () {
        transform.position = startPos;
        anim.SetBool ("Gallop", false);
        StopAllCoroutines ();
    }

    [Header ("Звук ускакивания")]
    public AudioClip gallopSound;

    /// <summary>
    /// Начать ускакивать
    /// </summary>
    public void RunAway () {
        anim.SetBool ("Gallop", true);

        if (AudioManager.instance != null) {
            AudioManager.instance.PlaySound (gallopSound);
        }

        if (runTime > 0)
            StartCoroutine (RunCorutine ()); //запускаем корутину для рестарта
    }

#if UNITY_EDITOR
    private void Update () {
        //подставляем лошадь под игрока
        if (!Application.isPlaying && playerController != null)
            transform.position = new Vector3 (transform.position.x, transform.position.y, playerController.transform.position.z);
    }
#endif

    private void OnTriggerEnter (Collider other) {
        //когда игрок в поле зрения, готовим его к лошади
        PlayerController.instance.PrepareForHorse ();
    }

    /// <summary>
    /// Корутина для рестарта игры
    /// </summary>
    /// <returns></returns>
    public IEnumerator RunCorutine () {
        float timer = 0;
        while (timer < runTime) {
            timer += Time.deltaTime;
            yield return null;
        }

        GameController.instance.Restart (); //перезапускаем уровень
    }
}