﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnCollisionEnterInvoker : MonoBehaviour {

    [System.Serializable]
    public class CollisionEvent : UnityEvent<Collision> { }

    public List<int> layers = new List<int> ();

    [SerializeField]
    public CollisionEvent onCollisionEnter;
    private void OnCollisionEnter (Collision other) {
        if (onCollisionEnter != null && layers.Contains (other.gameObject.layer))
            onCollisionEnter.Invoke (other);
    }
}