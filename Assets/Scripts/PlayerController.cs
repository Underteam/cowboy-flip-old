﻿using System.Collections;
using System.Collections.Generic;
using RootMotion.Dynamics;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour {
    [Header ("Основная физика игрока")]
    public Rigidbody baseRigidbody;
    [Header ("Аниматор")]
    public Animator anim;
    [Header ("Смешивание физики и анимаций")]
    public PuppetMaster puppetMaster;
    [Header ("Шляпа")]
    public Transform hat;
    public Transform hatParent;
    public Transform headHatPos;
    private Rigidbody hatRigidbody;

    public static PlayerController instance = null;
    private void Awake () {
        instance = this;
    }

    public UnityEvent onStart;
    private void Start () {
        hatRigidbody = hat.GetComponent<Rigidbody> ();
        hatRigidbody.isKinematic = true;

        puppetMaster.mode = PuppetMaster.Mode.Disabled; //отключаем физику, перед стартом

        baseRigidbody.isKinematic = true; //главный ригидбоди тоже выключен
        // baseRigidbody.SetMaxAngularVelocity (maxAngularVelocity);

        //добавляем событие на коллизию
        OnCollisionEnterInvoker[] collisionInvokers = GetComponentsInChildren<OnCollisionEnterInvoker> ();
        for (int i = 0; i < collisionInvokers.Length; i++) {
            collisionInvokers[i].layers.Clear ();
            collisionInvokers[i].onCollisionEnter.AddListener (CollisionWithSmth);
            collisionInvokers[i].layers.Add (0); //дефолт
            collisionInvokers[i].layers.Add (11); //касательно
            collisionInvokers[i].layers.Add (10); //батуты
        }

        StickyCollider[] stickyColliders = GetComponentsInChildren<StickyCollider> ();
        for (int i = 0; i < stickyColliders.Length; i++) {
            stickyColliders[i].onStick.AddListener (OnHorse);
        }

        if (onStart != null)
            onStart.Invoke ();
    }

    [Header ("Звук прыжка")]
    public AudioClip jumpSound;

    public UnityEvent onJump;
    /// <summary>
    /// Стартовый прыжок
    /// </summary>
    public void Jump () {
        Debug.Log ("Jump");
        anim.SetTrigger ("Jump"); //в анимации вызывается Jumped()

        if (AudioManager.instance != null) {
            AudioManager.instance.PlaySound (jumpSound);
        }

        if (onJump != null)
            onJump.Invoke ();
    }

    /// <summary>
    /// Был ли прыжок
    /// </summary>
    private bool jumped = false;
    private bool readyToFlip = false;
    public UnityEvent onJumped;
    public void Jumped () {
        if (jumped == true)
            return;

        Debug.Log ("Jumped");
        jumped = true; //засчитываем прыжок

        Vector3 localHatPos = hat.localPosition;
        Quaternion localRotation = hat.localRotation;
        hat.parent = hatParent;
        hat.localPosition = localHatPos;
        hat.localRotation = localRotation;

        puppetMaster.mode = PuppetMaster.Mode.Active; //делаем активной физику
        puppetMaster.behaviours[0].Activate ();

        StartCoroutine (NextFrameCoroutine (delegate {
            FreezeConstrains ();
            readyToFlip = true;
            GameController.instance.BeginGame ();
            if (onJumped != null)
                onJumped.Invoke ();
        }));

    }

    public void FreezeConstrains () {
        baseRigidbody.constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
    }

    public void UnfreezeConstrains () {
        baseRigidbody.constraints = RigidbodyConstraints.None;
    }

    public IEnumerator NextFrameCoroutine (System.Action act) {
        yield return null;
        yield return null;
        yield return null;
        yield return new WaitForEndOfFrame ();
        if (act != null)
            act.Invoke ();
    }

    [Header ("Размер сферы пересечения для запрета вращения")]
    public float overlapSphereSize = 10;

    [TextArea]
    public string descShown = "Смерть наступает при касании слоя Default. Слой Touchable ограничивает кручение.";
    private string desc = "Смерть наступает при касании слоя Default. Слой Touchable ограничивает кручение.";

    private void OnValidate () {
        descShown = desc;
    }

    private bool isFlipping = false;
    public bool IsFlipping {
        get {
            return isFlipping;
        }
    }

    [Space]
    [Header ("Рост персонажа")]
    [Tooltip ("Для силы кручения")]
    public float height = 2;
    [Header ("Сила кручения")]
    public float torquePower = 5;
    [Header ("Инерция кручния")]
    public float torqueInertion = 5;
    public float torqueInertionAfterKick = 5;
    [Header ("Скорость набора силы кручения")]
    public float acceliration = 2;
    float currentTorque = 0;

    bool isFlipByPlayer = false;
    public void Flip (float torq, bool useAcceleration, bool playerFlip) {
        if (!readyToFlip)
            return;

        isFlipByPlayer = playerFlip;

        if (useAcceleration)
            currentTorque = Mathf.Lerp (currentTorque, torq, Time.deltaTime * acceliration);
        else {
            Debug.Log (torq);
            currentTorque = torq;
        }
    }

    [Header ("Угол засчитывания флипа")]
    public float angleToFlip = 200;

    [HideInInspector]

    /// <summary>
    /// Событие на полный оборот
    /// </summary>
    public UnityEvent onFlip360;
    public void OnFlip360 () {
        flippingAngle = 0;
        if (onFlip360 != null)
            onFlip360.Invoke ();
    }

    /// <summary>
    /// Накрученный угол
    /// </summary>
    float flippingAngle = 0;
    private void Update () {

        // Time.timeScale = 0.5f;

        if (immortalTimer > 0)
            immortalTimer -= Time.deltaTime;

        if (readyToFlip && !onHorse) {

            float flipAngle = currentTorque * Time.deltaTime;
            baseRigidbody.transform.Rotate (new Vector3 (0, 0, -1) * flipAngle);

            if (!isFlipping) {
                currentTorque = Mathf.Lerp (currentTorque, 0, Time.deltaTime * ((isFlipByPlayer) ? torqueInertion : torqueInertionAfterKick));
            }

            // Debug.LogError ("FlipByPlayer " + isFlipByPlayer);
            if (isFlipping || isFlipByPlayer) {
                flippingAngle += Mathf.Abs (flipAngle); //добавляем сколько градусов накрутили
                // Debug.LogError ("flippingAngle " + flippingAngle);
                if (flippingAngle >= angleToFlip) {
                    OnFlip360 ();
                    flippingAngle = -(360 - angleToFlip);
                }
            }
        }

        if (IsDead || IsFalling || onHorse) {
            // {
            //     if (Input.GetKeyDown (KeyCode.Mouse0)) {
            //         if (!onHorse)
            //             GameController.instance.Restart ();
            //     }

            // }

            return;
        }

        if (!jumped) { //если прыжка не было то ждём нажатия кнопки мыши
            if (Input.GetKeyDown (KeyCode.Mouse0)) {
                Jump ();
            }
        } else {

            bool isMouseDown = Input.GetKey (KeyCode.Mouse0);
            anim.SetBool ("ForwardFlip", isMouseDown); //крутим, если кнопка нажата

            //начал крутиться, обнуляем накрученный угол
            if (isFlipping == false && isMouseDown)
                flippingAngle = 0;

            isFlipping = isMouseDown;

            if (isFlipping) {
                Collider[] colliders = Physics.OverlapSphere (transform.position, overlapSphereSize, LayerMask.GetMask ("Touchable"));
                if (colliders.Length <= 0) {
                    Flip (torquePower, true, true);
                }
            }
        }

    }

    [Header ("Бессмертие")]
    public bool immortal = false;

    [Header ("Время бессмертия после отскока")]
    public float immortalTimerAfterBounce = 1;
    float immortalTimer = 0;

    public bool isMortal {
        get {
            return immortalTimer <= 0 && !immortal;
        }
    }

    public void Bounce () {
        immortalTimer = immortalTimerAfterBounce;
    }

    // [Header ("Максимальная скорость кручения")]
    // public float maxTorque = 10;

    /// <summary>
    /// Придать ускорение всему телу
    /// </summary>
    /// <param name="velocity"></param>
    /// <param name="angularVelocity"></param>
    public void PuppetKick (Vector3 velocity, float angularVelocity) {
        foreach (Muscle m in puppetMaster.muscles) {
            m.rigidbody.velocity = velocity;
        }
        // Flip ((angularVelocity < 0) ? 1 : -1 * Mathf.Min (Mathf.Abs (angularVelocity), Mathf.Abs (maxTorque)), false);
        Flip (angularVelocity, false, false);
        Bounce ();
    }

    public void CollisionWithSmth (Collision collision) {
        // Debug.LogError ("Collision");
        if (!isMortal || !GameController.instance.IsGameStarted)
            return;

        if (collision.gameObject.layer == 11 && !IsFalling) //если столкновение с тем на чём можно ездить и персонаж не падает
            return;

        if (collision.gameObject.layer == 10 && !IsFalling) //если батут и не падает
            return;

        // if (collision.gameObject.layer == 0) {
        Kill ();
        // }
    }

    [Header ("Сила подкидывания шляпы")]
    public float hatDropForce = 1;
    /// <summary>
    /// Бросить шляпу
    /// </summary>
    public void DropHat () {
        if (hat == null || hat.parent == null)
            return;

        hat.parent = null;
        hatRigidbody.isKinematic = false;
        hatRigidbody.AddForce (hatDropForce * (Vector3.up).normalized, ForceMode.Impulse);
        hatRigidbody.AddTorque (hatDropForce * baseRigidbody.angularVelocity, ForceMode.Impulse);
    }

    [Header ("Цель камеры после смерти")]
    public Transform targetAfterDeath;
    private bool isDead = false;
    public bool IsDead {
        get {
            return isDead;
        }
    }

    public UnityEvent onDead;
    public void Kill () {
        if (isDead)
            return;

        isFlipping = false;
        puppetMaster.state = PuppetMaster.State.Dead;
        isDead = true;
        CameraFollow.instance.target = targetAfterDeath;
        UnfreezeConstrains ();
        DropHat ();
        if (onDead != null)
            onDead.Invoke ();
    }

    bool falling = false;
    public bool IsFalling {
        get {
            return falling;
        }
    }

    public UnityEvent onFall;
    [HideInInspector]
    public Bouncer bouncerFallOn = null;
    public void Fall (Bouncer bouncer) {
        isFlipping = false;
        falling = true;
        bouncerFallOn = bouncer;
        DropHat ();
        anim.SetBool ("ForwardFlip", false);
        anim.SetTrigger ("Fall");
        if (onFall != null)
            onFall.Invoke ();
    }

    private bool isPrepareForHorse = false;
    public bool IsPrepareForHorse {
        get {
            return isPrepareForHorse;
        }
    }
    /// <summary>
    /// Поза сажания на лошадь
    /// </summary>
    public void PrepareForHorse () {
        anim.SetBool ("HorseRiding", true);
        isPrepareForHorse = true;
    }

    bool onHorse = false;
    public void OnHorse () {
        if (onHorse)
            return;

        onHorse = true;

        anim.SetTrigger ("HatOn");
        Debug.Log ("HatOn");

        isFlipping = false;
        anim.SetBool ("ForwardFlip", false);

        immortal = true;
        puppetMaster.blendTime = 0.5f;
        transform.parent = HorseScript.instance.sittingPos;
        HorseScript.instance.RunAway ();
        puppetMaster.behaviours[0].gameObject.SetActive (false);
        CameraFollow.instance.target = HorseScript.instance.transform;
        CameraFollow.instance.updateY = false;

        GameController.instance.LevelFinish ();
        // CameraFollow.instance.target = HorseScript.instance.transform;
        // puppetMaster.
    }

    /// <summary>
    /// Рисование коллайдера ограничивающего кручение
    /// </summary>
    private void OnDrawGizmos () {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere (transform.position, overlapSphereSize);
    }
}