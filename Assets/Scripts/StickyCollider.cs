﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyCollider : MonoBehaviour {
    private Rigidbody rigid = null;
    public List<int> stickLayers = new List<int> ();
    private FixedJoint joint = null;

    private void Awake () {
        rigid = GetComponent<Rigidbody> ();
    }

    public UnityEngine.Events.UnityEvent onStick;

    public bool onlyOnTop = true;

    public Collider lastOther = null;

    private void OnCollisionEnter (Collision other) {
        // Debug.LogError ("Stricker " + other.gameObject.layer);

        if (!stickLayers.Contains (other.gameObject.layer) || joint != null)
            return;

        lastOther = other.transform.GetComponent<Collider> ();

        if (PlayerController.instance.IsFalling || PlayerController.instance.IsDead)
            return;

        if (onlyOnTop && PlayerController.instance.baseRigidbody.transform.position.y < other.transform.position.y + lastOther.bounds.extents.y)
            return;

        joint = gameObject.AddComponent<FixedJoint> ();
        Rigidbody otherRigid = other.gameObject.GetComponent<Rigidbody> ();
        if (otherRigid == null)
            return;

        joint.connectedBody = otherRigid;
        rigid.velocity = Vector3.zero;

        onStick.Invoke ();
        // LevelGenerator.instance.playerController.PuppetKick (Vector3.zero, 0);
    }
}