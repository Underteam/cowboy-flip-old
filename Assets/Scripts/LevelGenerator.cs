﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

[ExecuteInEditMode]
public class LevelGenerator : MonoBehaviour {

    #region Св-ва
    [Header ("Игрок")]
    public PlayerController playerController;

    [Header ("Расстояние между баунсерами по дефолту")]
    public float defaultDistance = 10;

    [Header ("Высота по дефолту")]
    public float defaultHeight = 0;

    [Header ("Удалять пустые")]
    public bool removeEmpty = false;

    [Header ("Автоматически расставлять точки")]
    public bool autoPos = false;

    [Space]
    [Header ("Рисовать путь")]
    public bool isDebug = true;

    [Header ("Рисовать стрелку")]
    public bool drawArrow = true;

    [Header ("Продолжительность построения пути")]
    public float debugTime = 10;

    [Header ("Период построения точек")]
    public float debugPeriod = 0.2f;

    [Header ("Время которое баунсер не активен, после соприкосновения")]
    public float bouncerInactiveTime = 2;

    [Space]

    [Header ("Стартовый баунсер")]
    public Bouncer startBouncer;

    [Header ("Лист отскоков")]
    public List<BouncerInfo> bouncers = new List<BouncerInfo> ();

    [Header ("Поворотов на бронзу")]
    public WinCondition flipCountBronze;
    [Header ("Поворотов на серебро")]
    public WinCondition flipCountSerebro;
    [Header ("Поворотов на золото")]
    public WinCondition flipCountGold;

    [System.Serializable]
    public class WinCondition {
        [Header ("Сколько поворотов")]
        public int flipCount = 0;
        [Header ("Нужно ли выполнить чтобы победить")]
        public bool needForWin = false;
        public string conditionName = "";
    }

    #endregion

    public WinCondition[] WinConditions {
        get {
            return new WinCondition[] { flipCountBronze, flipCountSerebro, flipCountGold };
        }
    }

    private void OnValidate () {
        WinCondition[] conditions = WinConditions;

        bool setAfterThat = false;
        for (int i = conditions.Length - 1; i >= 0; i--) {
            if (conditions[i].needForWin) {
                setAfterThat = true;
            }
            if (setAfterThat)
                conditions[i].needForWin = true;
        }
    }

    [System.Serializable]
    public class BouncerInfo {

        [Readonly]
        [Header ("Обьект")]
        public Bouncer bouncer;

        [Header ("Расстояние от предыдущего")]
        public float distanceFromPrev = 0;

        public BouncerInfo (LevelGenerator generator, Bouncer bouncer, float distanceFromPrev = 0, float defaultHeight = 0) {
            // this.bouncer = Instantiate (bouncer);
            bouncer.levelGenerator = generator;
            this.bouncer = bouncer;
            this.bouncer.transform.parent = generator.transform;
            this.bouncer.gameObject.isStatic = true;
            // this.bouncer.transform.position = new Vector3 (0, defaultHeight, 0);
            this.distanceFromPrev = distanceFromPrev;
        }
    }

    public static LevelGenerator instance = null;
    private List<Bouncer> bouncersObj = new List<Bouncer> ();
    private void Awake () {
        instance = this;
        bouncersObj = new List<Bouncer> (GameObject.FindObjectsOfType<Bouncer> ());
    }

    private void Start () {
        if (playerController != null)
            playerController.baseRigidbody.isKinematic = true;
    }

    public void MakeAllBouncersColliders () {
        Debug.Log ("MakeAllBouncersColliders");
        for (int i = 0; i < bouncers.Count; i++) {
            bouncers[i].bouncer.CachedCollider.isTrigger = false;
        }
    }

    [Header ("Задержка для того чтобы сделать всё коллайдерами")]
    float delayToMakeAllColliders = 0.5f;
    public void MakeAllBouncersCollidersDelayed () {
        Invoke ("MakeAllBouncersColliders", delayToMakeAllColliders);
    }

    [Header ("Ставить персонажа в точку старта")]
    public bool setPlayerToStart = true;

#if UNITY_EDITOR
    private void Update () {
        if (!Application.isPlaying) {
            if (setPlayerToStart && startBouncer != null && playerController != null) {
                playerController.transform.position = startBouncer.transform.position;
            }
        }
    }
#endif

    /// <summary>
    /// Сброс отскоков
    /// </summary>
    public void RestartBounces () {
        playerController = GameController.instance.playerController;
        for (int i = 0; i < bouncersObj.Count; i++) {
            bouncersObj[i].bounceIndex = 0;
            bouncersObj[i].playerController = playerController;
        }
        startBouncer.bounceIndex = 0;
    }
    // private void Update () {
    //     if (startBouncer == null)
    //         return;

    //     for (int i = 0; i < bouncers.Count; i++) {
    //         if (bouncers[i].bouncer == startBouncer || (removeEmpty && bouncers[i].bouncer == null)) {
    //             bouncers.RemoveAt (i);
    //             i--;
    //             continue;
    //         }

    //         if (bouncers[i].bouncer != null)
    //             bouncers[i].bouncer.levelGenerator = this;

    //         bouncers[i].bouncer.debugBounceIndex = -1;

    //         Vector3 pos = bouncers[i].bouncer.transform.position;
    //         Vector3 newPos = pos;
    //         if (autoPos) {
    //             Transform lastBouncerTransform = (i > 0) ? bouncers[i - 1].bouncer.transform : startBouncer.transform;
    //             newPos = lastBouncerTransform.position + bouncers[i].distanceFromPrev * Vector3.right;
    //             newPos.y = pos.y;
    //         }
    //         pos.z = 0;
    //         bouncers[i].bouncer.transform.position = newPos;
    //     }

    //     if (startBouncer != null && isDebug) {
    //         startBouncer.levelGenerator = this;
    //         startBouncer.debugBounceIndex = -1;
    //         startBouncer.DrawPath (startBouncer.transform.position);
    //     }
    // }
}