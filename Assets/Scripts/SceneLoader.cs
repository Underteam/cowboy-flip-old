﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {
    private void Awake () {
        int loadLevelIndex = PlayerPrefs.GetInt ("LevelIndex", 1);
        SceneManager.LoadScene (loadLevelIndex);
    }
}