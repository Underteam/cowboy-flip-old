﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RampBouncer : MonoBehaviour {

    public float velocity = 10;

    [Readonly]
    public float angle = 0;
    public List<StayCollider> collisionStay = new List<StayCollider> ();

    public class StayCollider {
        public Rigidbody rigid;
        public float allPathLength = 0;
        public float startMagnitude = 0;
        public StayCollider (Rigidbody rigid, float allPathLength, float startMagnitude) {
            this.rigid = rigid;
            this.allPathLength = allPathLength;
            this.startMagnitude = startMagnitude;
        }
    }

    public LevelGenerator levelGenerator = null;

    Collider cachedCollider = null;
    public Collider CachedCollider {
        get {
            if (cachedCollider == null) {
                cachedCollider = GetComponentInChildren<Collider> ();
            }

            return cachedCollider;
        }
    }

    MeshFilter cachedMeshFilter = null;
    public MeshFilter CachedMeshFilter {
        get {
            if (cachedMeshFilter == null) {
                cachedMeshFilter = GetComponentInChildren<MeshFilter> ();
            }

            return cachedMeshFilter;
        }
    }

    private void OnCollisionEnter (Collision other) {

        Rigidbody rigid = other.gameObject.GetComponent<Rigidbody> ();
        if (rigid == null)
            return;

        if (rigid.position.x < CachedCollider.bounds.min.x)
            return;

        collisionStay.Add (new StayCollider (rigid, endPoint.position.x - other.transform.position.x, rigid.velocity.magnitude));
    }

    private void OnCollisionExit (Collision other) {
        int index = collisionStay.FindIndex ((r) => r.rigid.gameObject == other.gameObject);
        if (index < 0)
            return;
        collisionStay.RemoveAt (index);
    }

    private void Update () {
        for (int i = 0; i < collisionStay.Count; i++) {
            collisionStay[i].rigid.velocity = collisionStay[i].rigid.velocity.normalized *
                Mathf.Lerp (collisionStay[i].startMagnitude, velocity, (endPoint.position.x - collisionStay[i].rigid.transform.position.x) * collisionStay[i].allPathLength);
        }

#if UNITY_EDITOR
        if (levelGenerator != null && endPoint != null && startPoint != null) {
            Vector3 angleVector = GetAngleVector ().normalized * velocity;
            List<Vector3> path = Bouncer.CalcPath (transform, endPoint.position, angleVector, levelGenerator.debugTime, levelGenerator.debugPeriod);

            Bouncer.DrawPathList (path);
            Bouncer.DrawArrow (endPoint.position, angleVector.normalized, 5, Color.cyan);
        }
#endif
    }

    private void OnDrawGizmos () {
        if (levelGenerator == null) {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube (transform.position, CachedCollider.bounds.size);
        }
    }

    public Vector3 GetAngleVector () {
        return (endPoint.position - startPoint.position).normalized;
    }

    public Transform startPoint;
    public Transform endPoint;
}