﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "LevelsInfo", menuName = "Bouncer/LevelsInfo")]
public class LevelsInfo : ScriptableObject {

    public List<string> complimentList = new List<string> ();
    public string GetCompliment () {
        if (complimentList.Count <= 0)
            return "";

        return complimentList[Random.Range (0, complimentList.Count)];
    }

    public List<int> levelsOnStage = new List<int> ();

    private void OnValidate () {
        for (int i = 0; i < levelsOnStage.Count; i++) {
            if (levelsOnStage[i] < 1)
                levelsOnStage[i] = 1;
        }
    }

    public int GetLevelIndex (int buildIndex) {

        int count = 0;
        for (int i = 0; i < levelsOnStage.Count; i++) {
            if (count + levelsOnStage[i] > buildIndex) {
                return i;
            }

            count += levelsOnStage[i];
        }

        return -1;
    }

    public int GetStageIndex (int levelIndex, int buildIndex) {
        int count = 0;
        for (int i = 0; i < levelIndex; i++) {
            count += levelsOnStage[i];
        }

        return buildIndex - count;
    }
}