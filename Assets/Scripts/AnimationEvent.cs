﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvent : MonoBehaviour {
    public UnityEngine.Events.UnityEvent onEvent;
    public void InvokeEvent () {
        if (onEvent != null)
            onEvent.Invoke ();
    }
}