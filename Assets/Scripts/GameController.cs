﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController instance;

    [Header ("Префаб игрока")]
    public PlayerController playerPrefab;

    [Readonly]
    [Header ("Игрок")]
    public PlayerController playerController;

    [Header ("Генератор уровня")]
    LevelGenerator generator;

    [Header ("Событие на начало игры")]
    public UnityEvent onGameBegin;

    [Header ("Информация о уровнях")]
    public LevelsInfo levelsInfo;

    [Header ("Панель с этапами")]
    public FormattedText levelText;

    [Header ("Панель с уровнями")]
    public LevelsPanel levelsPanel;

    [Header ("Панель поздравлений")]
    public CongratulationsUI congratulationsUI;

    [HideInInspector]
    private bool isGameStarted = false;
    public bool IsGameStarted {
        get {
            return isGameStarted;
        }
    }
    private void Awake () {
        Application.targetFrameRate = 60;

        instance = this;
        if (levelsPanel != null)
            levelsPanel.Init ();
    }

    [Header ("На старт игры")]
    public UnityEvent onGameStart;

    [Header ("На завершение уровня")]
    public UnityEvent onLevelFinish;

    [Header ("На завершение поражение")]
    public UnityEvent onDefead;

    [Header ("Когда игрок прыгает")]
    public UnityEvent onPlayerJump;

    [Header ("Количество флипов")]
    // [Readonly]
    public int flipCount = 0;
    [Header ("Событие на флип")]
    public UnityEvent onFlip;

    private void Start () {

        // Application.targetFrameRate = 

        PlayerPrefs.SetInt ("LevelIndex", SceneManager.GetActiveScene ().buildIndex);

        Init ();

        //записываем номер текущего уровня игры
        if (levelText != null && levelText.values.Count > 0)
            levelText.values[0] = (SceneManager.GetActiveScene ().buildIndex).ToString ();

        if (onGameStart != null)
            onGameStart.Invoke ();
    }

    public void Init () {
        flipCount = 0;

        generator = LevelGenerator.instance; //получаем ссылку на генератор уровня
        playerController = PlayerController.instance; //получаем ссылку на персонажа

        CameraFollow.instance.target = playerController.baseRigidbody.transform;
        CameraFollow.instance.updateY = true;
        isGameStarted = false;

        playerController.onFlip360.AddListener (PlayerFlip);
        playerController.onJump.AddListener (delegate { onPlayerJump.Invoke (); }); // на прыжок добавляем событие на прыжок
        playerController.onDead.AddListener (delegate {
            if (onDefead != null)
                StartCoroutine (VictoryPanel.DoSmthDelayed (victoryPanel.appearDelay, delegate { onDefead.Invoke (); }));
        });
    }

    /// <summary>
    /// Игрок совершил поворот
    /// </summary>
    public void PlayerFlip () {
        flipCount++;
        if (onFlip != null) {
            onFlip.Invoke ();
        }

        congratulationsUI.Show (flipCount);
    }

    [Header ("Начальная сила пинка")]
    public float startKickForce;

    /// <summary>
    /// Старт игры
    /// </summary>
    public void BeginGame () {
        Debug.Log ("BeginGame");

        if (generator.startBouncer == null)
            return;

        isGameStarted = true;

        #region Стартовый пинок
        playerController.baseRigidbody.isKinematic = false;
        generator.startBouncer.bounceIndex = 0;
        generator.startBouncer.Kick (playerController, playerController.transform.position - generator.startBouncer.bounces[0].GetAngleVector () * playerController.height * startKickForce);
        #endregion

        if (onGameBegin != null)
            onGameBegin.Invoke ();
    }

    /// <summary>
    /// Перезапуск сцены
    /// </summary>
    public void Restart () {
        onGameStart.Invoke ();
        // SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
        Destroy (playerController.gameObject);
        Destroy (playerController.hat.gameObject);
        playerController = Instantiate (playerPrefab, LevelGenerator.instance.startBouncer.transform.position, Quaternion.Euler (0, 90, 0));
        // playerController.transform.position = LevelGenerator.instance.startBouncer.transform.position;

        HorseScript.instance.Restart ();
        // congratulationsUI.gameObject.SetActive (false);

        Init ();
        LevelGenerator.instance.RestartBounces ();
        RepeaterManager.instance.Restart ();

        StopAllCoroutines ();
    }

    /// <summary>
    /// Старт следующей сцены
    /// </summary>
    public void NextScene () {
        Debug.Log ("Load next scene");
        int nextScene = SceneManager.GetActiveScene ().buildIndex + 1;
        if (SceneManager.GetActiveScene ().buildIndex + 1 < SceneManager.sceneCountInBuildSettings)
            SceneManager.LoadScene (nextScene);
        else {
            SceneManager.LoadScene (1);
        }
    }

    [Header ("Панель завершения игры")]
    public VictoryPanel victoryPanel;
    /// <summary>
    /// На завершение уровня
    /// </summary>
    public void LevelFinish () {

        StartCoroutine (VictoryPanel.DoSmthDelayed (victoryPanel.appearDelay, delegate {
            if (onLevelFinish != null)
                onLevelFinish.Invoke ();
            victoryPanel.gameObject.SetActive (true);
            victoryPanel.Init (LevelGenerator.instance, this);
        }));
        victoryPanel.SetInfoAboutCondition (LevelGenerator.instance);
    }
}