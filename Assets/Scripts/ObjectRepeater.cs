﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ObjectRepeater : MonoBehaviour {

    [Header ("Расстояние после которого обьект будет повторяться")]
    public float distanceToRepeat = 0;

    [Header ("Отступ")]
    public Vector3 offset = Vector3.zero;

    BoxCollider trigger;

    [Header ("Следующий объект")]
    public ObjectRepeater prevObj = null;

    private void Awake () {

    }

    private void Start () {
        if (!Application.isPlaying || trigger != null)
            return;

        trigger = gameObject.AddComponent<BoxCollider> ();

        trigger.size = Vector3.one * distanceToRepeat;
        trigger.center = offset;
        trigger.isTrigger = true;
    }

    public void SetNextPos () {
        transform.position = prevObj.transform.position + Vector3.right * (prevObj.distanceToRepeat + prevObj.offset.x);
    }

    private void OnDrawGizmos () {
        Gizmos.DrawWireCube (transform.position + offset, Vector3.one * distanceToRepeat);
    }

    [Header ("Учавствующие слои")]
    public List<int> usingLayers = new List<int> ();
    private void OnTriggerExit (Collider other) {
        if (!GameController.instance.IsGameStarted)
            return;

        if (usingLayers.Contains (other.gameObject.layer))
            StartCoroutine (SetNextPos (RepeaterManager.instance.setNextPosDelay, delegate { SetNextPos (); }));
    }

    public void StopAll () {
        StopAllCoroutines ();
    }

    public IEnumerator SetNextPos (float delay, System.Action act) {
        yield return new WaitForSeconds (delay);
        if (act != null)
            act.Invoke ();
    }
}