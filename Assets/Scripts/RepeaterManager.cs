﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeaterManager : MonoBehaviour {

    public int repeatersCount = 4;

    [HideInInspector]
    public List<ObjectRepeater> repeaters = new List<ObjectRepeater> ();

    int currentRepeaterIndex = 0;

    [HideInInspector]
    public int endRepeater;

    [Header ("Через сколько появится следующее поле")]
    public float setNextPosDelay = 5;

    public void SetCurrentRepeaterIndexToMax () {
        currentRepeaterIndex = repeaters.Count - 1;
    }

    public static RepeaterManager instance = null;

    private void Awake () {
        instance = this;
    }

    public List<Vector3> startPoses = new List<Vector3> ();

    private void Start () {
        ObjectRepeater baseRapeater = GameObject.FindObjectOfType<ObjectRepeater> ();
        if (baseRapeater == null)
            return;

        repeaters.Add (baseRapeater);
        startPoses.Add (baseRapeater.transform.position);
        for (int i = 1; i < repeatersCount; i++) {
            ObjectRepeater obj = Instantiate (baseRapeater);
            obj.prevObj = repeaters[i - 1];
            obj.transform.position = baseRapeater.transform.position + Vector3.right * (baseRapeater.distanceToRepeat + baseRapeater.offset.x) * i;
            repeaters.Add (obj);
            startPoses.Add (obj.transform.position);
        }

        baseRapeater.prevObj = repeaters[repeaters.Count - 1];
    }

    public void Restart () {
        for (int i = 0; i < repeaters.Count; i++) {
            repeaters[i].transform.position = startPoses[i];
            repeaters[i].StopAll ();
        }
    }
}