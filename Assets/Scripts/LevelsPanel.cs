﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelsPanel : MonoBehaviour {
    public StageUI stagePrefab;

    public Sprite levelIcon;
    public Sprite stageIcon;
    public void Init () {
        if (stagePrefab == null)
            return;

        LevelsInfo levelsInfo = GameController.instance.levelsInfo;

        for (int i = transform.childCount - 1; i >= 0; i--) {
            Destroy (transform.GetChild (i).gameObject);
        }

        int buildIndex = SceneManager.GetActiveScene ().buildIndex - 1; //индекс сцены в билде
        Debug.Log ("buildIndex " + buildIndex);
        int stageIndex = levelsInfo.GetLevelIndex (buildIndex); //уровень относительно этапов
        Debug.Log ("stageIndex" + stageIndex);
        if (stageIndex < 0)
            return;

        int levelOnStageIndex = levelsInfo.GetStageIndex (stageIndex, buildIndex); //какой сейчас этап (кружочек в картинке)
        Debug.Log ("levelOnStageIndex " + levelOnStageIndex);

        List<StageUI> stageUIs = new List<StageUI> ();
        for (int i = 0; i < levelsInfo.levelsOnStage[stageIndex] + 2; i++) {
            StageUI stageUI = Instantiate (stagePrefab, transform);

            bool isLevelPlank = (i == 0 || i == levelsInfo.levelsOnStage[stageIndex] + 1);
            stageUI.innerText.gameObject.SetActive (isLevelPlank);
            stageUI.toggleImage.gameObject.SetActive (!isLevelPlank && i - 1 == levelOnStageIndex);
            stageUI.mainImage.sprite = (isLevelPlank) ? levelIcon : stageIcon;

            if (!isLevelPlank && i - 1 < levelOnStageIndex)
                stageUI.mainImage.color = Color.green;

            stageUIs.Add (stageUI);
        }

        if (stageUIs.Count > 0) {
            stageUIs[0].innerText.text = (stageIndex + 1).ToString ();
            stageUIs[stageUIs.Count - 1].innerText.text = (stageIndex + 2).ToString ();
        }

    }
}