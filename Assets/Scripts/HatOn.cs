﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HatOn : MonoBehaviour {

    public float lerpTime = 1;

    public void PutHatOn () {
        Transform hat = PlayerController.instance.hat;

        if (hat == null)
            return;

        if (hat.parent == PlayerController.instance.headHatPos)
            return;

        hat.transform.parent = PlayerController.instance.headHatPos;
        StartCoroutine (LerpPos ());
    }

    public IEnumerator LerpPos () {
        Transform hat = PlayerController.instance.hat;
        Vector3 startPos = hat.transform.localPosition;
        Quaternion startRotation = hat.transform.localRotation;
        float timer = 0;
        while (timer < lerpTime) {
            hat.transform.localPosition = Vector3.Lerp (startPos, Vector3.zero, timer);
            hat.transform.localRotation = Quaternion.Lerp (startRotation, Quaternion.identity, timer);
            timer += Time.deltaTime;
            yield return null;
        }
        hat.transform.localPosition = Vector3.zero;
        hat.transform.localRotation = Quaternion.identity;
    }
}