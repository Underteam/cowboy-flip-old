﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

[System.Serializable]
public class StegoAudioClip {
    [Range (0, 1)]
    public float volume = 1;
    public AudioClip clip;
}

public class AudioManager : MonoBehaviour {

    public static AudioManager instance;

    public bool isSoundsOn = true, isMusicOn = true;
    public AudioSource audioSourceSounds, audioSourceMusic;

    // Use this for initialization

    private void Awake () {
        if (instance != null) {
            if (instance != this) {
                Destroy (instance.gameObject);
            }
        }

        Init ();
        instance = this;

        DontDestroyOnLoad (gameObject);
    }

    private void OnEnable () {

    }

    private void Start () {
        Init ();
    }

    public void Init () {
        Debug.Log ("Sound params loaded");
        isSoundsOn = PlayerPrefs.GetInt ("isSoundsOn", 1) == 1;
        isMusicOn = PlayerPrefs.GetInt ("isMusicOn", 1) == 1;

        if (audioSourceSounds != null)
            audioSourceSounds.enabled = isSoundsOn;
        if (audioSourceMusic != null)
            audioSourceMusic.enabled = isMusicOn;
    }

    public void SwitchSound () {
        isSoundsOn = !isSoundsOn;
        PlayerPrefs.SetInt ("isSoundsOn", (isSoundsOn) ? 1 : 0);
        if (audioSourceSounds != null)
            audioSourceSounds.enabled = isSoundsOn;
    }

    /// <summary>
    /// Событие которое вызывается по переключении звука
    /// </summary>
    public UnityEvent switchSoundEvent;
    public void SwitchMusic () {
        isMusicOn = !isMusicOn;
        PlayerPrefs.SetInt ("isMusicOn", (isMusicOn) ? 1 : 0);
        if (audioSourceMusic != null)
            audioSourceMusic.enabled = isMusicOn;

        switchSoundEvent.Invoke ();
    }

    public void PlaySound (AudioClip clip) {
        // Debug.LogError ("Play sound");
        if (audioSourceSounds == null || clip == null)
            return;
        if (audioSourceSounds.enabled) {
            // Debug.LogError ("Played");
            audioSourceSounds.PlayOneShot (clip);
        }
    }

    public void PlaySound (StegoAudioClip stegoClip) {
        if (audioSourceSounds == null || stegoClip == null)
            return;
        if (audioSourceSounds.enabled)
            audioSourceSounds.PlayOneShot (stegoClip.clip, stegoClip.volume);
    }
}