﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI;

public class ExtendedButton : Button {

	private void Awake () {
		OverrideGraphic ();
	}

	public List<MaskableGraphic> allGraphic = new List<MaskableGraphic> ();
	protected override void DoStateTransition (Selectable.SelectionState state, bool instant) {
		base.DoStateTransition (state, instant);

		Color color;
        switch (state)
        {
            case Selectable.SelectionState.Normal:
                color = this.colors.normalColor;
                break;
            case Selectable.SelectionState.Highlighted:
                color = this.colors.highlightedColor;
                break;
            case Selectable.SelectionState.Pressed:
                color = this.colors.pressedColor;
                break;
            case Selectable.SelectionState.Disabled:
                color = this.colors.disabledColor;
                break;
            default:
                color = Color.black;
                break;
        }
        if (base.gameObject.activeInHierarchy)
        {
            switch (this.transition)
            {
                case Selectable.Transition.ColorTint:
                    DoGraphicTransition(color * this.colors.colorMultiplier, instant);
                    break;
            }
        }
	}

	public void DoGraphicTransition (Color targetColor, bool instant) {
		for (int i = 0; i < allGraphic.Count; i++) {
			allGraphic[i].CrossFadeColor(targetColor, (!instant) ? this.colors.fadeDuration : 0f, true, true);
		}
	}

	[ContextMenu ("OverrideGraphic")]
	public void OverrideGraphic () {
		allGraphic = new List<MaskableGraphic> (gameObject.GetComponentsInChildren<MaskableGraphic> ());
		allGraphic.Remove (this.targetGraphic as MaskableGraphic);
	}
}