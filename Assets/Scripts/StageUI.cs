﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageUI : MonoBehaviour {
    public Image mainImage;
    public Image toggleImage;
    public Text innerText;
}