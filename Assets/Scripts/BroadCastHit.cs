﻿using System.Collections;
using System.Collections.Generic;
using RootMotion.Dynamics;
using UnityEngine;

public class BroadCastHit : MonoBehaviour {
    public MuscleCollisionBroadcaster broadcaster;

    public float pin;
    public Vector3 force;
    public PuppetMaster puppetMaster;

    [EditorButton]
    public void Test () {
        // broadcaster.puppetMaster = puppetMaster;
        foreach (Muscle m in puppetMaster.muscles) {
            m.rigidbody.AddForce (force.normalized * pin, ForceMode.Impulse);
        }

    }
}