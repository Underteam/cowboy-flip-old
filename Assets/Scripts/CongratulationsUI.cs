﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CongratulationsUI : MonoBehaviour {
    public Text congratulationsText;
    private Animator anim;
    private void Awake () {
        anim = GetComponent<Animator> ();
    }

    public void Show (int flipCount) {
        // gameObject.SetActive (true);
        anim.SetTrigger ("Appear");
        congratulationsText.text = flipCount.ToString () + "X\n" + GameController.instance.levelsInfo.GetCompliment () + "!";
    }

    // public void Hide () {
    //     gameObject.SetActive (false);
    // }
}