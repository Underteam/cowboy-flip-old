﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveUI : MonoBehaviour {
    public Image mark;
    public Text conditionText;

    public void Init () {

    }

    public void SetActive (bool mode) {
        for (int i = 0; i < transform.childCount; i++) {
            transform.GetChild (i).gameObject.SetActive (mode);
        }
    }
}