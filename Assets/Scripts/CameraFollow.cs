﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public Transform target;
    public Vector3 offset;
    public Quaternion rotation;

    public bool updateY = true;

    public bool lerpOn = false;
    public float lerpSpeed = 10;

    public static CameraFollow instance = null;

    private void Awake () {
        instance = this;
    }

    [EditorButton ("Сохранить позицию")]
    public void Save () {
        if (target == null)
            return;

        offset = transform.position - target.position;
        rotation = transform.rotation;
    }

    [EditorButton]
    public void Adapt () {
        if (target == null)
            return;

        Vector3 targetPos = (target.position + offset);
        targetPos = new Vector3 (targetPos.x, (updateY) ? targetPos.y : transform.position.y, targetPos.z);

        if (lerpOn)
            transform.position = Vector3.Lerp (transform.position, targetPos, Time.deltaTime * lerpSpeed);
        else
            transform.position = targetPos;

        transform.rotation = rotation;
    }

    private void LateUpdate () {
        Adapt ();
    }
}