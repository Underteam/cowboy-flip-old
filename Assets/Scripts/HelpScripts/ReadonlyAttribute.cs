﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ReadonlyAttribute : PropertyAttribute {
	public ReadonlyAttribute () { }
}

[System.Serializable]
public class Editable<T> {
	public T value;
	public bool editable = true;

	public Editable () {

	}

	public Editable (T _value, bool editable_ = true) {
		value = _value;
		editable = editable_;
	}
}