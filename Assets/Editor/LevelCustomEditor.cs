﻿using System.Collections;
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (LevelGenerator))]
public class LevelCustomEditor : Editor {

    private LevelGenerator levelGenerator;
    private GUIStyle rectTextStyle;
    public void OnEnable () {
        levelGenerator = target as LevelGenerator;

        rectTextStyle = new GUIStyle ();
        rectTextStyle.alignment = TextAnchor.MiddleCenter;
        rectTextStyle.fontStyle = FontStyle.Bold;
    }

    public override void OnInspectorGUI () {
        DrawDefaultInspector ();
        EditorGUILayout.Space ();
        DropAreaGUI ();
    }

    public void DropAreaGUI () {
        Event evt = Event.current;
        Rect drop_area = GUILayoutUtility.GetRect (0.0f, 50.0f, GUILayout.ExpandWidth (true));
        GUI.Box (drop_area, "Сюда кидать новые баунсеры");

        switch (evt.type) {
            case EventType.DragUpdated:
            case EventType.DragPerform:
                if (!drop_area.Contains (evt.mousePosition))
                    return;

                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                if (evt.type == EventType.DragPerform) {
                    DragAndDrop.AcceptDrag ();

                    foreach (Object dragged_object in DragAndDrop.objectReferences) {

                        Bouncer bouncer = null;

                        if (dragged_object.GetType () == typeof (GameObject))
                            bouncer = ((GameObject) dragged_object).GetComponent<Bouncer> ();

                        if (bouncer != null) {
                            if (bouncer != null && bouncer.gameObject.scene.IsValid ()) {
                                levelGenerator.bouncers.Add (new LevelGenerator.BouncerInfo (levelGenerator, bouncer, levelGenerator.defaultDistance, levelGenerator.defaultHeight));
                            }
                        }
                    }
                }
                break;
        }
    }
}