﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer (typeof (ReadonlyAttribute))]
public class ReadonlyAttributeDrawer : PropertyDrawer {
    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {

        // var readonlyAttribure = (ReadonlyAttribute) attribute;
        // var propertyValue = GetValue (property.serializedObject);

        // EditorGUI.BeginDisabledGroup ((bool) propertyValue);
        EditorGUI.BeginDisabledGroup (true);
        EditorGUI.PropertyField (position, property);
        EditorGUI.EndDisabledGroup ();

    }

    private object GetValue (object value) {
        Type t = value.GetType ();
        if (t.IsGenericType) {
            if (t.GetGenericTypeDefinition () == typeof (Editable<>)) //check the object is our type
            {
                //Get the property value
                return t.GetProperty ("value").GetValue (value, null);
            }
        }
        return value;
    }

}